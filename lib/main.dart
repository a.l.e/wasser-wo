import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:location/location.dart';
import 'package:map_controller/map_controller.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:shared_preferences/shared_preferences.dart';
void main() {
  runApp(WoWasserApp());
}

class WoWasserApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeView(title: 'Where is the water?'),
    );
  }
}

class HomeView extends StatefulWidget {
  HomeView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  LatLng _center;
  LatLng _currentLocation;
  MapController _mapController;
  StatefulMapController _statefulMapController;
  StreamSubscription<StatefulMapControllerStateChange> sub;

  var _location = Location();
  final _searchRadius = [100, 200, 500, 1000, 2000, 5000];
  int _searchRadiusIndex = 3;

  @override
  void initState() {
    _mapController = MapController();
    _statefulMapController = StatefulMapController(mapController: _mapController);
    _statefulMapController.onReady.then((change) => initLocation());
    sub = _statefulMapController.changeFeed.listen((change) => setState(() {}));
    super.initState();
  }

  @override
  void dispose() {
    sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Builder(
        builder: (BuildContext context){
          return SafeArea(
            child: FlutterMap(
              options: MapOptions(
                center: _center, // LatLng(51.5, -0.09),
                zoom: 13.0,
                onLongPress:(coord) => setLocation(context, coord),
              ),
              mapController: _mapController,
              layers: [
                TileLayerOptions(
                  urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                  subdomains: ['a', 'b', 'c']
                ),
                // _statefulMapController.tileLayer,
                MarkerLayerOptions(
                  markers: _statefulMapController.markers,
                ),
              ],
            )
          );
        },
      ),
      floatingActionButton: Builder(builder: (BuildContext context) {
        return Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(width: 30),
            // Expanded(
            //   child: FloatingActionButton(
            //     heroTag: null,
            //     onPressed: () => setCurrentLocation(context),
            //     tooltip: 'Current location',
            //     child: Icon(Icons.my_location),
            //   ),
            // ),
            FloatingActionButton(
              heroTag: null,
              onPressed: () => setCurrentLocation(context),
              tooltip: 'Current location',
              child: Icon(Icons.my_location),
            ),
            Spacer(),
            Container(
              decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.4) // RGBA
              ),
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  getSearchRadiusText(),
                  style: TextStyle(fontSize: 25),
                ),
              )
            ),
            Padding(padding: EdgeInsets.only(left: 20)),
            FloatingActionButton(
              heroTag: null,
              onPressed: () => canShrinkRadius() ? shrinkRadius(context) : null,
              backgroundColor: canShrinkRadius() ? Colors.blue : Colors.grey[350],
              tooltip: 'Decrement',
              child: Icon(Icons.remove),
            ),
            Padding(padding: EdgeInsets.only(left: 20)),
            FloatingActionButton(
              heroTag: null,
              onPressed: () => canGrowRadius() ? growRadius(context) : null,
              backgroundColor: canGrowRadius() ? Colors.blue : Colors.grey[350],
              tooltip: 'Increment',
              child: Icon(Icons.add),
            ),
          ]
        );
      }),
    );
  }

  initLocation() async {
    final prefs = await SharedPreferences.getInstance();

    _currentLocation = LatLng(prefs.getDouble('latitude') ?? 47.3769, prefs.getDouble('longitude') ?? 8.5417);
    _searchRadiusIndex = prefs.getInt('radius') ?? 3;

    setState(() {
      _mapController.move(_currentLocation, 13.0);
    });

    addCurrentMarkers();
  }

  setCurrentLocation(BuildContext context) async {
    moveToLocation(context, await getCurrentLocation());
  }

  moveToLocation(BuildContext context, LatLng coord) async {
    _currentLocation = coord;

    setState(() {
      _mapController.move(coord, 13.0);
    });

    await storeLocation(_currentLocation);

    await addCurrentMarkers();
    Scaffold.of(context).showSnackBar(SnackBar(content: Text('There are ${_statefulMapController.markers.length - 1} water points.'),));
  }

  setLocation(BuildContext context, LatLng coord) async {
    _currentLocation = coord;

    storeLocation(_currentLocation);

    await addCurrentMarkers();

    Scaffold.of(context).showSnackBar(SnackBar(content: Text('There are ${_statefulMapController.markers.length - 1} water points.'),));
  }

  canShrinkRadius() => _searchRadiusIndex > 0;

  shrinkRadius(BuildContext context) async {
    if (canShrinkRadius()) {
      setState(() {
        _searchRadiusIndex--;
        storeRadius(_searchRadiusIndex);
      });
    }
    await addCurrentMarkers();

    Scaffold.of(context).showSnackBar(SnackBar(content: Text('There are ${_statefulMapController.markers.length - 1} water points.'),));
  }

  canGrowRadius() => _searchRadiusIndex < _searchRadius.length - 1;

  growRadius(BuildContext context) async {
    if (canGrowRadius()) {
      setState(() {
        _searchRadiusIndex++;
        storeRadius(_searchRadiusIndex);
      });
    }
    await addCurrentMarkers();

    Scaffold.of(context).showSnackBar(SnackBar(content: Text('There are ${_statefulMapController.markers.length - 1} water points.'),));
  }

  getSearchRadiusText() {
    final radius = _searchRadius[_searchRadiusIndex];
    return (radius < 1000) ? '$radius m' : '${radius ~/ 1000} km';
  }

  storeLocation(LatLng coord) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble('latitude', coord.latitude);
    prefs.setDouble('longitude', coord.longitude);
  }

  storeRadius(int radiusIndex) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('radius', radiusIndex);
  }

  getCurrentLocation() async {
    bool _serviceEnabled;
    PermissionStatus permissionGranted;
    LocationData locationData;

    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    permissionGranted = await _location.hasPermission();
    if (permissionGranted == PermissionStatus.denied) {
      permissionGranted = await _location.requestPermission();
      if (permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    locationData = await _location.getLocation();
    return LatLng(locationData.latitude, locationData.longitude);
  }

  addCurrentMarkers() async {

    await _statefulMapController.removeMarkers(names: _statefulMapController.namedMarkers.keys.toList());

    final url = 'https://z.overpass-api.de/api/interpreter'; // up to 10000 queries per day
    final response = await http.post(url,
     body: '[out:json][timeout:25]; node [amenity=drinking_water] (around:${_searchRadius[_searchRadiusIndex]},${_currentLocation.latitude},${_currentLocation.longitude}); out;'
    );

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      // print(jsonResponse['elements'].length);
      jsonResponse['elements'].forEach((e) {
        // print('${e['id']} ${e['lat']}, ${e['lon']}');
        _statefulMapController.addMarker(
          name: e['id'].toString(),
          marker: Marker(
            point: LatLng(e['lat'], e['lon']),
            builder: (BuildContext context) {
              return const Icon(
                  Icons.location_on,
                  color: Colors.lightBlue,
                  );
            }),
          );
      });
      _statefulMapController.addMarker(
        name: 'current location',
        marker: Marker(
          point: _currentLocation,
          builder: (BuildContext context) {
            return const Icon(
                Icons.location_on,
                color: Colors.red,
                );
          }),
        );
    } else {
      throw Exception('Failed to load album');
    }
  }
}

