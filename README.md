# Wo Wasser – Where is the water?

Flutter App for finding water points around you.

## Development notes

### OpenStreetMap

Querying:

- <https://overpass-turbo.osm.ch/>: A platform for querying the OSM API
- Water points 1 km around 
- <https://github.com/fleaflet/flutter_map>


`flutter_map`:

- setting the current location: <https://stackoverflow.com/a/60532719/5239250>
- when tracking the current location, avoiding that the location keeps getting updated by using a service:
  - <https://stackoverflow.com/questions/60920079/location-onchange-function-keeps-getting-called-even-when-location-is-static-fl>
  - <https://medium.com/flutter-community/build-a-location-service-in-flutter-367a1b212f7a>
- an example: <https://github.com/fleaflet/flutter_map/tree/master/example>

### Settings

- Storing the position in the settings and retrivieing it on start (instead of the current location)

### Toast / Snackbar

Since the widgets are implemented directly in the same context as the scaffold we need to create a `Builder` widget that allows to use `Scaffold.of(context)` (<https://api.flutter.dev/flutter/material/Scaffold/of.html>).
